# Documentation
# -------------

furo==2021.4.11b34
myst-parser[linkify]<0.16
sphinx-autobuild==2021.3.14
sphinx-copybutton<1
sphinxext-opengraph<1
jinja2<3.1
