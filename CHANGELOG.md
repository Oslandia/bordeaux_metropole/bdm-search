# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.2.1 - 2024-02-03

- Connexion à la base avec ou sans port.

## 1.2.0 - 2024-04-16

- Ajout d'un timer sur le le highlight 
- Ajout d'un highlight sur la bbox pour les tables qui n'ont pas de géométries
- Ajout d'une variable d'environnement pour l'authentification
## 1.1.1 - 2023-12-14

- Fix sur le highlight et la gestion des fonds de cartes
- Changement dans l'authentification à la bd pour gérer le stockage de l'id dans QGIS

## 1.1.0 - 2023-11-24

- Ajout d'un highlight court sur un objet localisé via le WPS

## 1.0.0-beta2 - 2021-08-03

- Correction du bug permettant la recherche par identifiant
- Centralisation des paramètres dans l'interface de configuration du plugin
- La request_key des services web de Bordeaux peut être récupérée à l'aide d'une variable d'environnement
- Nettoyage et suppression du code inutile

## 1.0.0-beta1 - 2021-07-29

- Ajout de la connexion au service de recherche du WPS de BDM
- Gestion des thèmes et des alias des layers depuis le service `dico_theme`
- Gestion de l'authentification basique depuis le authManager de QGIS pour une utilisation hors intranet (id:bdmAuth)
- Zoom sur l'extent lors de la sélection

## 0.1.0 - 2021-06-14

- first Versioning

