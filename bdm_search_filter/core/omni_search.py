#########################################################################
#
# Copyright (C) 2016 Bordeaux Métropole
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

# standard library
from xml.dom import minidom

# PyQGIS
from qgis.core import Qgis, QgsMessageLog, QgsProviderRegistry

# project
from bdm_search_filter.toolbelt.wpsclient import WPSClient
from bdm_search_filter.toolbelt.preferences import PlgOptionsManager


def getNodeValueNS(xml, ns, name):
    elements = [
        x
        for x in xml.getElementsByTagNameNS(ns, name)
        if x.nodeType == minidom.Node.ELEMENT_NODE
    ]
    for element in elements:
        texts = [x for x in element.childNodes if x.nodeType == minidom.Node.TEXT_NODE]
        for text in texts:
            return text.nodeValue
    return None


class OmniSearchException(Exception):
    """
    Classe d'exception
    """

    def __init__(self, *args, **kwargs):
        super(OmniSearchException, self).__init__(*args, **kwargs)


class OmniSearchPoint(object):
    def __init__(self, literal):
        self._x = None
        self._y = None

        literals = literal.split(u" ")
        if len(literals) == 2 and literals[0] and literals[1]:
            self._x = float(literals[0])
            self._y = float(literals[1])

    @property
    def x(self):
        """
        @return: La valeur X
        @rtype: long
        """
        return self._x

    @property
    def y(self):
        """
        @return: La valeur y
        @rtype: long
        """
        return self._y


class OmniSearchBound(object):
    """
    Classe bound
    """

    def __init__(self, xml, ns=PlgOptionsManager.get_plg_settings().ns_gml_uri):
        self._lowerCorner = None
        self._upperCorner = None

        # Parse XML
        self._parseXml(xml, ns)

    @property
    def lowerCorner(self):
        """
        @return: Le champ lowerCorner
        @rtype: str
        """
        return self._lowerCorner

    @property
    def upperCorner(self):
        """
        @return: Le champ upperCorner
        @rtype: str
        """
        return self._upperCorner

    def _parseXml(self, xml, ns):
        envelopes = xml.getElementsByTagNameNS(ns, "Envelope")
        if envelopes.length > 0:
            self._lowerCorner = OmniSearchPoint(
                getNodeValueNS(envelopes[0], ns, "lowerCorner")
            )
            self._upperCorner = OmniSearchPoint(
                getNodeValueNS(envelopes[0], ns, "upperCorner")
            )


class OmniSearchResult(object):
    """
    Classe de résultat de la recherche
    """

    def __init__(
        self,
        xml,
        ns=PlgOptionsManager.get_plg_settings().ns_bm_uri,
        nsGml=PlgOptionsManager.get_plg_settings().ns_gml_uri,
    ):
        self._nsGml = nsGml
        self._gid = None
        self._libelle = None
        self._couche = None
        self._pertinence = None
        self._boundedBy = None
        self._source = None
        self._commune = None

        # Parsing XML
        self._parseXml(xml, ns)

    @property
    def gid(self):
        """
        @return: Le champ GID
        @rtype: str
        """
        return self._gid

    @property
    def libelle(self):
        """
        @return: Le champ LIBELLE
        @rtype: str
        """
        return self._libelle

    @property
    def theme(self):
        """
        @return: Le theme calculé à partir du champ COUCHE
        @rtype: str
        """
        index = self._couche.find("_")
        if self._couche[:index] == "CF" or self._couche[:index] == "CP":
            return "PUBLIC_CF_CP"
        elif self._couche[:index] == "FV" or self._couche[:index] == "DP":
            return "PUBLIC_FV_DP"
        else:
            return "PUBLIC_{}".format(self._couche[:index])

    @property
    def couche(self):
        """
        @return: Le champ COUCHE
        @rtype: str
        """
        return self._couche

    @property
    def pertinence(self):
        """
        @return: Le champ PERTINENCE
        @rtype: int
        """
        if self._pertinence and self._pertinence.isdigit():
            return int(self._pertinence)
        return 0

    @property
    def boundedBy(self):
        """
        @return: Le champ boundedBy
        @rtype: str
        """
        return self._boundedBy

    @property
    def source(self):
        """
        @return: Le champ SOURCE
        @rtype: str
        """
        return self._source

    @property
    def commune(self):
        """
        @return: Le champ COMMUNE
        @rtype: str
        """
        dicoCommu = {
            "33003": u"Ambarès",
            "33004": u"Ambès",
            "33013": u"Artigues",
            "33032": u"Bassens",
            "33039": u"Bègles",
            "33056": u"Blanquefort",
            "33063": u"Bordeaux",
            "33065": u"Bouliac",
            "33069": u"Le Bouscat",
            "33075": u"Bruges",
            "33096": u"Carbon blanc",
            "33119": u"Cenon",
            "33162": u"Eysines",
            "33167": u"Floirac",
            "33192": u"Gradignan",
            "33200": u"Le Haillan",
            "33249": u"Lormont",
            "33273": u"Martignas",
            "33281": u"Mérignac",
            "33312": u"Parempuyre",
            "33318": u"Pessac",
            "33376": u"Saint Aubin de Medoc",
            "33434": u"Saint Louis de Montferrand",
            "33449": u"Saint Médard en Jalles",
            "33487": u"Saint Vincent de Paul",
            "33519": u"Le Taillan Medoc",
            "33522": u"Talence",
            "33550": u"Villenave d''Ornon",
        }

        if self._commune in dicoCommu:
            return dicoCommu[self._commune]
        else:
            return self._commune

    def to_json(self):
        return (
            "{{"
            '"gid": "{}",'
            '"libelle": "{}",'
            '"couche": "{}",'
            '"pertinence": "{}",'
            '"boundedBy": {{'
            '"lowerCorner": {{"x": {},"y": {}}},'
            '"upperCorner": {{"x": {},"y": {}}}'
            "}},"
            '"source": "{}",'
            '"commune": "{}"'
            "}}"
        ).format(
            self._gid,
            self._libelle,
            self._couche,
            self._pertinence,
            self._boundedBy._lowerCorner._x,
            self._boundedBy._lowerCorner._y,
            self._boundedBy._upperCorner._x,
            self._boundedBy._upperCorner._y,
            self._source,
            self._commune,
        )

    def _parseXml(self, xml, ns):
        self._gid = getNodeValueNS(xml.documentElement, ns, "GID")
        self._libelle = getNodeValueNS(xml.documentElement, ns, "LIBELLE")
        self._couche = getNodeValueNS(xml.documentElement, ns, "COUCHE")
        self._pertinence = getNodeValueNS(xml.documentElement, ns, "PERTINENCE")
        self._source = getNodeValueNS(xml.documentElement, ns, "SOURCE")
        self._commune = getNodeValueNS(xml.documentElement, ns, "COMMUNE")

        bounds = xml.documentElement.getElementsByTagNameNS(self._nsGml, "boundedBy")
        if bounds.length > 0:
            self._boundedBy = OmniSearchBound(bounds[0])


class OmniSearch(object):
    """
    Classe de recherche
    """

    def __init__(
        self,
        searchText,
        searchLimit=10,
        searchLayer=None,
        url=PlgOptionsManager.get_plg_settings().request_url,
        key=PlgOptionsManager.get_plg_settings().request_key,
    ):
        self._url = url
        self._key = key
        self._identifier = "recherche"
        self._results = []
        self._searchText = {}
        if searchText:
            self._searchText["input"] = searchText
        if searchLimit:
            self._searchText["limit"] = searchLimit
        if searchLayer:
            self._searchText["couches"] = searchLayer

        # Lancement de la recherche
        self._wpsExecuteOmniSearch()

    @property
    def results(self):
        """
        @return: Les résultats
        @rtype: list<OmniSearchResult>
        """
        return self._results

    def _wpsExecuteOmniSearch(self):
        wpsClient = WPSClient(self._url, self._key)
        try:
            execution = wpsClient.execute(self._identifier, self._searchText)
            for result in execution.processOutputs:
                if result.identifier == "result":
                    for data in [x for x in result.data if x.mimeType == "text/xml"]:
                        try:
                            rawData = data.data  # type: str
                            cleanData = """
                                <result xmlns:bm=\"{nsBm}\" xmlns:gml=\"{nsGml}\">
                                {rawData}
                                </result>
                            """.format(
                                rawData=rawData,
                                nsBm=PlgOptionsManager.get_plg_settings().ns_bm_uri,
                                nsGml=PlgOptionsManager.get_plg_settings().ns_gml_uri,
                            )
                            xml = minidom.parseString(cleanData)
                        except Exception:
                            raise OmniSearchException(
                                "Erreur de parsing du xml rendu par %s"
                                % self._identifier
                            )
                        # Ajout des résultats
                        self._results.append(OmniSearchResult(xml))
        except Exception as inst:
            QgsMessageLog.logMessage(
                "OmniSearch erreur : {}".format(inst.args), "Messages", Qgis.Info
            )
            self._results = []


class ResultHighlights():

    def getLayerData(self, nom):
        theme = []
        couche = []
        geomFieldName = []
        """
        Récupère les informations détaillées et les relations de la couche
        """
        wpsClient = WPSClient(
            PlgOptionsManager.get_plg_settings().request_url,
            PlgOptionsManager.get_plg_settings().request_key,
        )

        execution = wpsClient.execute("dico_couches", {"couche": nom})
        for layer in execution.processOutputs:
            if layer.identifier == "couche":
                for data in [x for x in layer.data if x.mimeType == "text/xml"]:
                    try:
                        xml = minidom.parseString(data.data)
                    except Exception:
                        raise Exception(
                            "Erreur de parsing du xml rendu par dico_couches"
                        )

                    # Récupération des attributs et des relations
                    for attribut in xml.getElementsByTagNameNS(
                        PlgOptionsManager.get_plg_settings().ns_bm_uri, "Theme"
                    ):
                        theme.append(
                            {
                                "nom": attribut.getAttribute("nom"),
                                "source": attribut.getAttribute("source"),
                            }
                        )
                        for attribut in xml.getElementsByTagNameNS(
                            PlgOptionsManager.get_plg_settings().ns_bm_uri, "Couche"
                        ):
                            couche.append(
                                    {
                                        "nom": attribut.getAttribute("nom"),
                                        "alias": attribut.getAttribute("alias"),
                                        "geomType": attribut.getAttribute("geomType"),
                                        "objets": attribut.getAttribute("objets"),
                                    }
                                )
                            for attribut in xml.getElementsByTagNameNS(
                                PlgOptionsManager.get_plg_settings().ns_bm_uri, "Attribut"
                                ):

                                if (
                                    attribut.hasAttribute("type")
                                    and attribut.getAttribute("type") == "GEOMETRIE"
                                ):
                                    geomFieldName = attribut.getAttribute("nom")
                                                           
                            couche = couche[0]['nom']
                            theme = theme[0]['nom']
                            geomFieldName = geomFieldName
        return (couche, theme, geomFieldName)
  
    def getSearchGeom(self, gid, couche, theme, geom_name):
   
        # Connexion à la bd et exécution du SQL. Retourne la geom de l'objet 
        host = PlgOptionsManager.get_plg_settings().oracle_host
        port = PlgOptionsManager.get_plg_settings().oracle_port,
        database = PlgOptionsManager.get_plg_settings().oracle_database,
        oracle_auth = PlgOptionsManager.get_plg_settings().oracle_auth
        if host and port[0]:
            self.dbconn = f"host={host}/{database[0]} port={port[0]} authcfg='{oracle_auth}'"
        else :
            self.dbconn = f"dbname='{database[0]}' authcfg='{oracle_auth}'"
        md = QgsProviderRegistry.instance().providerMetadata('oracle')
        conn = md.createConnection(self.dbconn, {})
        geom_ly = conn.executeSql(f"SELECT SDO_UTIL.TO_WKTGEOMETRY({geom_name}) FROM {theme}.QG_{couche} WHERE GID={gid}")

        return geom_ly
