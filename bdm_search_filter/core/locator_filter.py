#! python3  # noqa: E265

"""
    Main plugin module.
"""

import json

# standard library
import logging

# PyQGIS
from qgis.core import (
    Qgis,
    QgsCoordinateReferenceSystem,
    QgsCoordinateTransform,
    QgsFeedback,
    QgsLocatorContext,
    QgsLocatorFilter,
    QgsLocatorResult,
    QgsMessageLog,
    QgsProject,
    QgsRectangle,
    QgsGeometry,
    QgsPointXY

)
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtWidgets import QWidget
from qgis.utils import iface
from qgis.gui import QgsHighlight
from PyQt5.QtGui import QColor
# project
from bdm_search_filter.__about__ import __title__
from bdm_search_filter.core.omni_search import OmniSearch, ResultHighlights
from bdm_search_filter.core.themes import ThemesDict
from bdm_search_filter.toolbelt import PlgLogger, PlgOptionsManager
from PyQt5.QtCore import QTimer

# ############################################################################
# ########## Globals ###############
# ##################################

logger = logging.getLogger(__name__)

# ############################################################################
# ########## Classes ###############
# ##################################


class BordeauxOmnisearchFilter(QgsLocatorFilter):
    """QGIS Locator Filter subclass.

    :param iface: An interface instance that will be passed to this class which \
    provides the hook by which you can manipulate the QGIS application at run time.
    :type iface: QgisInterface
    """

    def __init__(self, iface: QgisInterface):
        self.iface = iface
        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager.get_plg_settings()
        self.hgl=None

        super(QgsLocatorFilter, self).__init__()

    def name(self) -> str:
        """Returns the unique name for the filter. This should be an untranslated \
        string identifying the filter.

        :return: filter unique name
        :rtype: str
        """
        return self.__class__.__name__

    def hasConfigWidget(self) -> bool:
        """Should return True if the filter has a configuration widget.

        :return: configuration widget available
        :rtype: bool
        """
        return True

    def clone(self) -> QgsLocatorFilter:
        """Creates a clone of the filter. New requests are always executed in a clone \
        of the original filter.

        :return: clone of the actual filter
        :rtype: QgsLocatorFilter
        """
        return BordeauxOmnisearchFilter(self.iface)

    def displayName(self) -> str:
        """Returns a translated, user-friendly name for the filter.

        :return: user-friendly name to be displayed
        :rtype: str
        """
        return __title__

    def prefix(self) -> str:
        """Returns the search prefix character(s) for this filter. Prefix a search with \
        these characters will restrict the locator search to only include results from \
        this filter.

        :return: search prefix for the filter
        :rtype: str
        """
        return "bdx"

    def fetchResults(
        self, search: str, context: QgsLocatorContext, feedback: QgsFeedback
    ):
        """Retrieves the filter results for a specified search string. The context \
        argument encapsulates the context relating to the search (such as a map extent \
        to prioritize). \

        Implementations of fetchResults() should emit the resultFetched() signal \
        whenever they encounter a matching result. \
        Subclasses should periodically check the feedback object to determine whether \
        the query has been canceled. If so, the subclass should return from this method \
        as soon as possible. This will be called from a background thread unless \
        flags() returns the QgsLocatorFilter.FlagFast flag.

        :param search: text entered by the end-user into the locator line edit
        :type search: str
        :param context: [description]
        :type context: QgsLocatorContext
        :param feedback: [description]
        :type feedback: QgsFeedback
        """
        # ignore if search terms is inferior than 3 chars or equal to the prefix
        if (
            len(search) < self.plg_settings.min_search_length
            or search.rstrip() == self.prefix
        ):
            return

        # request
        try:
            omniSearch = OmniSearch(search)
        except Exception as err:
            self.log(message=err, log_level=1, push=True)
            return

        # process response
        try:
            # load result from OmniSearch
            omniResults = omniSearch.results
            # loop on features in json collection
            for res in omniResults:
                result = QgsLocatorResult()
                result.filter = self
                label = "{}{commune}".format(
                    res.libelle,
                    commune=(" - {}".format(res.commune) if res.commune else ""),
                )
                result.displayString = label
                result.group = "{}{}".format(
                    ("[IDENT] " if res.source == "RECHERCHE_IDENT" else ""),
                    ThemesDict.instance().getLayerByLayerName(res.couche).alias,
                )

                # use the json full item as userData, so all info is in it:
                result.userData = res.to_json()
     
                self.resultFetched.emit(result)
        except Exception as inst:
            QgsMessageLog.logMessage(
                "Fetch Results erreur : {}".format(inst.args), "Messages", Qgis.Info
            )
            return

    def triggerResult(self, result: QgsLocatorResult):
        """Triggers a filter result from this filter. This is called when one of the \
        results obtained by a call to fetchResults() is triggered by a user. \
        The filter subclass must implement logic here to perform the desired operation \
        for the search result. E.g. a file search filter would open file associated \
        with the triggered result.

        :param result: [description]
        :type result: QgsLocatorResult
        """
        if __debug__:
            self.log(
                message=f"DEBUG - Selected address: {result.displayString}", log_level=4
            )
        doc = json.loads(result.userData)

        # Réinitialisation de QgsHighlight() et sélection de la geom par QgsHighlight()

        if self.hgl:
            iface.mapCanvas().scene().removeItem(self.hgl)
            self.hgl=None
    
        oracle_connexion = ResultHighlights()
        couche, theme, geomFieldName = oracle_connexion.getLayerData(doc["couche"])

        # gestion des tables sans géométrie pour lesquelles on highlight autour du point fourni par doc["boundedBy"]
        if geomFieldName:
            geom_ly = oracle_connexion.getSearchGeom(doc["gid"], couche, theme, geomFieldName)
            geom_ly = QgsGeometry.fromWkt(geom_ly[0][0])
        else:
            lowerCorner = QgsPointXY(doc["boundedBy"]['lowerCorner']['x']-20, doc["boundedBy"]['lowerCorner']['y']-20)
            upperCorner = QgsPointXY(doc["boundedBy"]['upperCorner']['x']+20, doc["boundedBy"]['upperCorner']['y']+20)
            geom = QgsRectangle(lowerCorner, upperCorner)
            geom_ly = QgsGeometry.fromRect(geom)

        layer = iface.activeLayer()
        # Récupérer le système de coordonnées de référence (CRS) du canvas
        canvas = iface.mapCanvas()
        mapRenderer = canvas.mapSettings()
        target_crs = mapRenderer.destinationCrs()
        source_crs = QgsCoordinateReferenceSystem(3945)
        transform = QgsCoordinateTransform(source_crs, target_crs, QgsProject.instance())
        geom_ly.transform(transform)
        self.hgl = QgsHighlight(canvas, geom_ly, layer)
        self.hgl.setColor(QColor(255, 0, 0, 255))
        self.hgl.setWidth(1)
        self.hgl.show()
       
        bbox = QgsRectangle(
            doc["boundedBy"]["lowerCorner"]["x"],
            doc["boundedBy"]["lowerCorner"]["y"],
            doc["boundedBy"]["upperCorner"]["x"],
            doc["boundedBy"]["upperCorner"]["y"],
        )

        bufferedBbox = bbox.buffered(50)

        dest_crs = QgsProject.instance().crs()
        results_crs = QgsCoordinateReferenceSystem.fromEpsgId(3945)
        aTransform = QgsCoordinateTransform(
            results_crs, dest_crs, QgsProject.instance()
        )
        bufferedBboxProjected = aTransform.transform(bufferedBbox)
        self.iface.mapCanvas().setExtent(bufferedBboxProjected)
        self.iface.mapCanvas().refresh()
   
        # Supression du highlight au bout de 3 secondes
        self.timer = QTimer()
        highlight_timer = int(self.plg_settings.highlight_timer)*1000
        self.timer.start(highlight_timer)
        self.timer.timeout.connect(self.remove_highlight)

    def remove_highlight(self):
        self.timer.stop()
        self.hgl.hide()

    def tr(self, message) -> str:
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str

        :returns: Translated version of message.
        :rtype: str
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate(self.__class__.__name__, message)

    def openConfigWidget(self, parent: QWidget = None):
        """Opens the configuration widget for the filter (if it has one), with the \
        specified parent widget. self.hasConfigWidget() must return True.

        :param parent: [description], defaults to None
        :type parent: QWidget, optional
        """
        self.iface.showOptionsDialog(
            parent=parent, currentPage=f"mOptionsPage{__title__}"
        )
