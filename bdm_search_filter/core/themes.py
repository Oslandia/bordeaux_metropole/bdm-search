#########################################################################
#
# Copyright (C) 2016 Bordeaux Métropole
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

# pylint: disable=E0611

"""
Package de gestion des objets EP
"""
from xml.dom import minidom

from qgis.core import Qgis, QgsMessageLog
from qgis.PyQt.QtCore import QObject

from bdm_search_filter.toolbelt.preferences import PlgOptionsManager
from bdm_search_filter.toolbelt.wpsclient import WPSClient, WPSClientException


class Singleton(QObject):
    """
    Singleton
    """

    __instance = None

    @classmethod
    def instance(cls):
        """
        Récupère l'instance du singleton
        """
        return cls.__instance

    @classmethod
    def clearInstance(cls):
        """
        Nettoie l'instance du singleton
        """
        cls.__instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = QObject.__new__(cls, *args, **kwargs)
        return cls.__instance


class ThemeLayer(object):
    """
    Classe des layers liés aux thèmes
    """

    def __init__(self, xml):
        """
        Constructeur
        @param xml: Chaine XML
        @type xml: str
        """
        self._xml = xml
        self._name = None
        self._alias = None
        self._geomType = None

        # Parsing XML
        self._parseLayer(self._xml)

    @property
    def name(self):
        """
        @return: Le nom de la couche
        @rtype: str
        """
        return self._name

    @property
    def alias(self):
        """
        @return: L'alias de la couche
        @rtype: str
        """
        return self._alias if self._alias is not None else self._name

    @property
    def geomType(self):
        """
        @return: Le type de géométrie de la couche
        @rtype: str
        """
        return self._geomType

    def _parseLayer(self, xml):
        if xml.hasAttribute("nom"):
            self._name = xml.getAttribute("nom")
        if xml.hasAttribute("geomType"):
            self._geomType = xml.getAttribute("geomType")
        if xml.hasAttribute("alias"):
            self._alias = xml.getAttribute("alias")


class Theme(object):
    """
    Classe de gestion des thèmes
    """

    def __init__(self, xml):
        """
        Contructeur
        @param xml: Chaine XML
        @type xml: str
        """
        self._name = None
        self._alias = None
        self._layers = []
        self._xml = xml

        # Parsing XML
        self._parseTheme(xml)

    @property
    def name(self):
        """
        @return: Le nom du thème
        @rtype: str
        """
        return self._name

    @property
    def alias(self):
        """
        @return: L'alias du thème
        @rtype: str
        """
        return self._alias

    def getLayers(self):
        """
        Retourne la liste des couches du theme
        @return: La liste des couches
        @rtype: list(EPLayer)
        """
        return self._layers

    def hasLayer(self, name):
        """
        Retourne True si la couche est présente dans la liste des couches du thème
        @param name: Le nom de la couche
        @type name: str
        """
        for layer in self._layers:
            if layer.name == name:
                return True
        return False

    def getLayer(self, name):
        """
        Retourne une couche par son nom
        @param name: Le nom de la couche
        @type name: str
        @return: La couche
        @rtype: ThemeLayer
        """
        for layer in self._layers:
            if layer.name == name:
                return layer
        return None

    def _parseTheme(self, xml):
        documentElement = xml.documentElement
        if documentElement.hasAttribute("nom"):
            self._name = documentElement.getAttribute("nom")
        if documentElement.hasAttribute("alias"):
            self._alias = documentElement.getAttribute("alias")
        for child in documentElement.childNodes:
            if child.localName == "Couche":
                self._layers.append(ThemeLayer(child))


class ThemesDict(Singleton):
    """
    Singleton de gestion des themes
    """

    def __init__(
        self,
        url=PlgOptionsManager.get_plg_settings().request_url,
        key=PlgOptionsManager.get_plg_settings().request_key,
        ns=PlgOptionsManager.get_plg_settings().ns_bm_uri,
        theme=None,
    ):
        """
        Constructeur
        @param url: L'url du service WPS
        @type url: str
        @param key: La clé d'accés au service WPS
        @type key: str
        @param epSourcePublic: La source
        @type epSourcePublic: EPSource
        @param connectionModeler: La connexion au dictionnaire privé
        @type connectionModeler: QSqlOracleConnection
        @param ns: Namespace
        @type ns: str
        @param theme: Le nom du thème
        @type theme: str
        """
        self._url = url
        self._key = key
        self._themes = []
        self._ns = ns

        self._wpsExecuteDictThemes(theme)

    def _wpsExecuteDictThemes(self, theme):
        """
        Récupère la liste des themes publics
        """
        if theme:
            inputs = {"theme": theme}
        else:
            inputs = None

        try:
            wpsClient = WPSClient(self._url, self._key)
            execution = wpsClient.execute("dico_themes", inputs)
            for theme in execution.processOutputs:
                if theme.identifier == "theme":
                    for data in [x for x in theme.data if x.mimeType == "text/xml"]:
                        try:
                            xml = minidom.parseString(data.data)
                        except Exception:
                            QgsMessageLog.logMessage(
                                "Erreur de parsing du xml par dico_themes",
                                "Messages",
                                Qgis.Info,
                            )
                        # Ajout des themes
                        self._themes.append(Theme(xml))
        except WPSClientException as error:
            QgsMessageLog.logMessage(
                "_wpsExecuteDictThemes erreur : {}".format(error.args),
                "Messages",
                Qgis.Info,
            )

    def getThemes(self):
        """
        Récupère la liste des themes publics
        @return: La liste des thèmes publics
        @rtype: list<EspacePublicTheme>
        """
        return self._themes

    def getTheme(self, name):
        """
        Récupère un theme public par son nom
        @param name: le nom du thème
        @type name: str
        @return: Le theme
        @rtype: EspacePublicTheme
        """
        for theme in self._themes:
            if theme.name == name:
                return theme
        return None

    def getThemeByLayerName(self, name):
        """
        Récupère le theme associé au nom d'une couche
        @param name: le nom de la couche
        @type name: str
        @return: Le nom du theme
        @rtype: str
        """
        for theme in self._themes:
            for layer in theme.getLayers():
                if layer.name == name:
                    return theme
        return None

    def getLayerByLayerName(self, name):
        """
        Récupère l'alias associé à une couche
        @param name: le nom de la couche
        @type name: str
        @return: L'alias'
        @rtype: str
        """
        for theme in self._themes:
            for layer in theme.getLayers():
                if layer.name == name:
                    return layer
        return None
