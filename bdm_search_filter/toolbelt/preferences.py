#! python3  # noqa: E265

"""
    Plugin settings.
"""

# standard
import os
from typing import NamedTuple

# PyQGIS
from qgis.core import QgsSettings

# package
import bdm_search_filter.toolbelt.log_handler as log_hdlr
from bdm_search_filter.__about__ import __title__, __version__

# ############################################################################
# ########## Classes ###############
# ##################################


class PlgSettingsStructure(NamedTuple):
    """Plugin settings structure and defaults values."""

    # misc
    debug_mode: bool = False
    version: str = __version__

    # network
    http_content_type: str = "text/xml"
    http_user_agent: str = f"{__title__}/{__version__}"
    min_search_length: int = 3
    request_url: str = "https://data.bordeaux-metropole.fr/wps"
    request_url_query: str = "limit=10"
    request_key: str = os.getenv("CARTOQGIS_REQUEST_KEY")
    wps_service: str = "WPS"
    wps_version: str = "1.0.0"
    auth_id: str = ""
    highlight_timer : str = "3"
    # database
    oracle_id_column: str = "GID"
    oracle_default_srid: str = "3945"
    oracle_host: str = ""
    oracle_database: str = ""
    oracle_port: str = ""
    oracle_auth: str = ""

    # namespace
    ns_bm_uri: str = "http://data.bordeaux-metropole.fr"
    ns_gml_uri: str = "http://www.opengis.net/gml"
    ns_ows_uri: str = "http://www.opengis.net/ows/1.1"
    ns_wps_uri: str = "http://www.opengis.net/wps/1.0.0"


class PlgOptionsManager:
    """Plugin options manager."""

    @staticmethod
    def get_plg_settings() -> PlgSettingsStructure:
        """Load and return plugin settings as a dictionary. \
        Useful to get user preferences across plugin logic.

        :return: plugin settings
        :rtype: PlgSettingsStructure
        """
        settings = QgsSettings()
        settings.beginGroup(__title__)

        options = PlgSettingsStructure(
            # misc
            debug_mode=settings.value(key="debug_mode", defaultValue=False, type=bool),
            version=settings.value(key="version", defaultValue=__version__, type=str),
            # network
            http_content_type=settings.value(
                key="http_content_type",
                defaultValue="text/xml",
                type=str,
            ),
            http_user_agent=settings.value(
                key="http_user_agent",
                defaultValue=f"{__title__}/{__version__}",
                type=str,
            ),
            min_search_length=settings.value(
                key="min_search_length",
                defaultValue=2,
                type=int,
            ),
            request_url=settings.value(
                key="request_url",
                defaultValue="https://data.bordeaux-metropole.fr/wps/",
                type=str,
            ),
            request_url_query=settings.value(
                key="request_url_query",
                defaultValue="limit=10",
                type=str,
            ),
            request_key=settings.value(
                key="request_key",
                defaultValue=os.getenv("CARTOQGIS_REQUEST_KEY"),
                type=str,
            ),
            auth_id=settings.value(
                key="auth_id",
                defaultValue="",
                type=str,
            ),
            wps_service=settings.value(
                key="wps_service",
                defaultValue="WPS",
                type=str,
            ),
            wps_version=settings.value(
                key="wps_version",
                defaultValue="1.0.0",
                type=str,
            ),
            ns_bm_uri=settings.value(
                key="ns_bm_uri",
                defaultValue="http://data.bordeaux-metropole.fr",
                type=str,
            ),
            ns_gml_uri=settings.value(
                key="ns_gml_uri",
                defaultValue="http://www.opengis.net/gml",
                type=str,
            ),
            ns_ows_uri=settings.value(
                key="ns_ows_uri",
                defaultValue="http://www.opengis.net/ows/1.1",
                type=str,
            ),
            ns_wps_uri=settings.value(
                key="ns_wps_uri",
                defaultValue="http://www.opengis.net/wps/1.0.0",
                type=str,
            ),
            oracle_id_column=settings.value(
                key="oracle_id_column",
                defaultValue="GID",
                type=str,
            ),
            oracle_default_srid=settings.value(
                key="oracle_default_srid",
                defaultValue="3945",
                type=str,
            ),
            highlight_timer=settings.value(
                key="highlight_timer",
                defaultValue="3000",
                type=str,
            ),
            oracle_host=settings.value(
                key="oracle_host",
                defaultValue="",
                type=str,
            ),
            oracle_database=settings.value(
                key="oracle_database",
                defaultValue="",
                type=str,
            ),
            oracle_port=settings.value(
                key="oracle_port",
                defaultValue="",
                type=str,
            ),
            oracle_auth=settings.value(
                key="oracle_auth",
                defaultValue="",
                type=str,
            ),
        )
        settings.endGroup()

        return options

    @staticmethod
    def get_value_from_key(key: str, default=None, exp_type=None):
        """Load and return plugin settings as a dictionary. \
        Useful to get user preferences across plugin logic.

        :return: plugin settings value matching key
        """
        if not hasattr(PlgSettingsStructure, key):
            log_hdlr.PlgLogger.log(
                message="Bad settings key. Must be one of: {}".format(
                    ",".join(PlgSettingsStructure._fields)
                ),
                log_level=1,
            )
            return None

        settings = QgsSettings()
        settings.beginGroup(__title__)

        try:
            out_value = settings.value(key=key, defaultValue=default, type=exp_type)
        except Exception as err:
            log_hdlr.PlgLogger.log(
                message="Error occurred trying to get settings: {}.Trace: {}".format(
                    key, err
                )
            )
            out_value = None

        settings.endGroup()

        return out_value

    @classmethod
    def set_value_from_key(cls, key: str, value):
        """Load and return plugin settings as a dictionary. \
        Useful to get user preferences across plugin logic.

        :return: plugin settings value matching key
        """
        if not hasattr(PlgSettingsStructure, key):
            log_hdlr.PlgLogger.log(
                message="Bad settings key. Must be one of: {}".format(
                    ",".join(PlgSettingsStructure._fields)
                ),
                log_level=2,
            )
            return False

        settings = QgsSettings()
        settings.beginGroup(__title__)

        try:
            settings.setValue(key, value)
            out_value = True
        except Exception as err:
            log_hdlr.PlgLogger.log(
                message="Error occurred trying to set settings: {}.Trace: {}".format(
                    key, err
                )
            )
            out_value = False

        settings.endGroup()

        return out_value

    @classmethod
    def save_from_object(cls, plugin_settings_obj: PlgSettingsStructure):
        """Load and return plugin settings as a dictionary. \
        Useful to get user preferences across plugin logic.

        :return: plugin settings value matching key
        """
        settings = QgsSettings()
        settings.beginGroup(__title__)

        for k, v in plugin_settings_obj._asdict().items():
            cls.set_value_from_key(k, v)

        settings.endGroup()
