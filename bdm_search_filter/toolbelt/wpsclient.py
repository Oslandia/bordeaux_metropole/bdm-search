#########################################################################
#
# Copyright (C) 2016 Bordeaux Métropole
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

"""
Module de gestion des connexions à un serveur WPS
"""

from xml.dom import minidom

from qgis.PyQt.Qt import QUrl

from bdm_search_filter.toolbelt.network_manager import NetworkRequestsManager
from bdm_search_filter.toolbelt.ows import (
    OperationMetadata,
    Output,
    Process,
    ProcessDescription,
    ServiceIdentification,
    Status,
)
from bdm_search_filter.toolbelt.preferences import PlgOptionsManager


class WPSExceptionReport(Exception):
    """
    Classe d'exception
    """

    def __init__(self, code, message):
        super(WPSExceptionReport, self).__init__()
        self.code = code
        self.message = message


class WPSClientException(Exception):
    def __init__(self, *args, **kwargs):
        super(WPSClientException, self).__init__(*args, **kwargs)


class WPSReader(object):
    """
    Classe d'envoi et de réception de requêtes WPS
    """

    def __init__(
        self,
        key,
        version=PlgOptionsManager.get_plg_settings().wps_version,
        service=PlgOptionsManager.get_plg_settings().wps_service,
    ):
        self._key = key
        self._version = version
        self._service = service

    def _readFromStringXML(self, xml):
        # Parsing xml
        return minidom.parseString(xml)

    def _readFromUrl(self, url, data, method="GET"):
        """
        Envoi de la requête et réception de la réponse
        @param url: Url du service
        @param data: Données de la requête
        @param method: Méthode de la requête (GET, POST, ...)
        @return: Instance minidom de la réponse
        """
        if method == "GET":
            data["key"] = self._key
            data["version"] = self._version
            data["service"] = self._service

            # Requête HTTP
            try:
                opener = NetworkRequestsManager()
                requestUrl = opener.encodeUrl(url, data)
                response = opener.get_url(QUrl(requestUrl))
            except Exception:
                raise WPSClientException(
                    "Erreur lors de l'ouverture de l'url {requestUrl}".format(
                        requestUrl=requestUrl
                    )
                )

            # Parsing en xml de la réponse
            return minidom.parse(response)


class WPSCapabilitiesReader(WPSReader):
    """
    Classe d'envoi et de réception d'une requête GetCapabilities
    """

    def __init__(
        self,
        key,
        version=PlgOptionsManager.get_plg_settings().wps_version,
        service=PlgOptionsManager.get_plg_settings().wps_service,
    ):
        super(WPSCapabilitiesReader, self).__init__(
            key, version=version, service=service
        )

    def readFromStringXML(self, xml):
        """
        Lecture du xml résultant du getCapabilities
        @param xml: Flux XML
        @type xml: str
        """
        return self._readFromStringXML(xml)

    def readFromUrl(self, url):
        """
        Envoi de la requête et réception de la réponse
        @param url: Url du service
        @return: Instance minidom de la réponse
        """
        return self._readFromUrl(url, {"request": "GetCapabilities"})


class WPSDescribeProcessReader(WPSReader):
    """
    Classe d'envoi et de réception d'une requête Describe
    """

    def __init__(
        self,
        key,
        version=PlgOptionsManager.get_plg_settings().wps_version,
        service=PlgOptionsManager.get_plg_settings().wps_service,
    ):
        super(WPSDescribeProcessReader, self).__init__(
            key, version=version, service=service
        )

    def readFromStringXML(self, xml):
        """
        Lecture du xml résultant du Describe
        @param xml: Flux XML
        @type xml: str
        """
        return self._readFromStringXML(xml)

    def readFromUrl(self, url, identifier):
        """
        Envoi de la requête et réception de la réponse
        @param url: Url du service
        @return: Instance minidom de la réponse
        """
        return self._readFromUrl(
            url, {"request": "DescribeProcess", "identifier": identifier}
        )


class WPSExecutionReader(WPSReader):
    """
    Classe d'envoi et de réception d'une requête Execute
    """

    def __init__(
        self,
        key,
        version=PlgOptionsManager.get_plg_settings().wps_version,
        service=PlgOptionsManager.get_plg_settings().wps_service,
    ):
        super(WPSExecutionReader, self).__init__(key, version=version, service=service)

    def readFromStringXML(self, xml):
        """
        Lecture du xml résultant du Execute
        @param xml: Flux XML
        @type xml: str
        """
        return self._readFromStringXML(xml)

    def readFromUrl(self, url, data):
        """
        Envoi de la requête et réception de la réponse
        @param url: Url du service
        @param data: Données de la requête
        @return: Instance minidom de la réponse
        """
        return self._readFromUrl(url, data)


class WPSExecution(object):
    """
    Classe de traitement d'une requête Execute
    """

    def __init__(
        self,
        key,
        identifier,
        version=PlgOptionsManager.get_plg_settings().wps_version,
        service=PlgOptionsManager.get_plg_settings().wps_service,
    ):
        self.key = key
        self.identifier = identifier
        self.version = version
        self.service = service
        self.status = None
        self.processOutputs = []

    def _parseExceptionReport(self, xml):
        exceptionElement = xml.documentElement.getElementsByTagName("Exception")[0]
        exceptionTextElement = exceptionElement.getElementsByTagName("ExceptionText")[0]
        exceptionCode = exceptionElement.getAttribute("exceptionCode")
        exceptionText = exceptionTextElement.firstChild.nodeValue
        raise WPSExceptionReport(exceptionCode, exceptionText)

    def _parseExecutionResponse(self, xml):
        elements = [
            x
            for x in xml.documentElement.childNodes
            if x.nodeType == minidom.Node.ELEMENT_NODE
        ]
        for element in elements:
            if element.localName == "Status":
                self.status = Status(
                    element, ns=PlgOptionsManager.get_plg_settings().ns_wps_uri
                )
            if element.localName == "ProcessOutputs":
                ns_wps = PlgOptionsManager.get_plg_settings().ns_wps_uri
                ns_ows = PlgOptionsManager.get_plg_settings().ns_ows_uri
                for child in element.getElementsByTagNameNS(ns_wps, "Output"):
                    self.processOutputs.append(Output(child, ns=ns_ows))

    def _parseResponse(self, xml):
        """
        Parsing de la réponse
        @param xml: Instance minidom de la réponse
        """
        rootTagName = xml.documentElement.localName
        if rootTagName == "ExceptionReport":
            self._parseExceptionReport(xml)
        elif rootTagName == "ExecuteResponse":
            self._parseExecutionResponse(xml)
        elif rootTagName == "ProcessDescriptions":
            self._parseExecutionResponse(xml)

    def fromStringXML(self, xml):
        reader = WPSExecutionReader(
            self.key, version=self.version, service=self.service
        )
        response = reader.readFromStringXML(xml)
        self._parseResponse(response)

    def sendRequest(self, url, inputs=None):
        """
        Préparation de la requête
        @param url: Url du service
        @param inputs: Données de la requête
        @return: Instance minidom de la réponse
        """
        data = {"request": "Execute", "identifier": self.identifier}

        dataInputs = NetworkRequestsManager.encodeInputs(inputs)
        data["datainputs"] = dataInputs

        reader = WPSExecutionReader(
            self.key, version=self.version, service=self.service
        )
        response = reader.readFromUrl(url, data)
        self._parseResponse(response)


class WPSClient(object):
    """
    Classe de communication avec un service WPS
    """

    def __init__(
        self,
        url,
        key,
        version=PlgOptionsManager.get_plg_settings().wps_version,
        service=PlgOptionsManager.get_plg_settings().wps_service,
    ):
        self.key = key
        self.url = url
        self.version = version
        self.service = service
        self._capabilities = None
        self.identification = None
        self.processDescriptions = []
        self.operations = []
        self.processes = []
        self.processOutputs = []

    def getCapabilities(self, xml=None):
        """
        Récupère les capacités du service WPS
        @param xml: Flux XML
        @type xml: str
        """
        reader = WPSCapabilitiesReader(
            self.key, version=self.version, service=self.service
        )
        if xml is None:
            self._capabilities = reader.readFromUrl(self.url)
        else:
            self._capabilities = reader.readFromStringXML(xml)

        self._parseCapabilities(self._capabilities)

    def describeProcess(self, identifier, xml=None):
        """
        Décrit un processus
        @param identifier: Identifiant du processus
        @type identifier: str
        @param xml: Flux XML
        @type xml: str
        """
        reader = WPSDescribeProcessReader(
            self.key, version=self.version, service=self.service
        )
        if xml is None:
            processDescriptions = reader.readFromUrl(self.url, identifier)
        else:
            processDescriptions = reader.readFromStringXML(xml)

        self._parseProcessDescriptions(processDescriptions)

    def execute(self, identifier, inputs=None, xml=None):
        """
        Exécute une requête du service WPS
        @param identifier: Identifiant de la requête
        @type identifier: str
        @param inputs: Données à faire passer
        @type inputs: dict
        @return: Instance WPSExecution
        @param xml: Flux XML
        @type xml: str
        @rtype: WPSExecution
        """
        execution = WPSExecution(
            self.key, identifier, version=self.version, service=self.service
        )
        if xml is None:
            execution.sendRequest(self.url, inputs=inputs)
        else:
            execution.fromStringXML(xml)

        return execution

    def _parseProcessDescriptions(self, xml):
        """
        Parse les capacités du service WPSClient
        @param xml: Instance minidom des capacités
        """
        elements = [
            x
            for x in xml.documentElement.childNodes
            if x.nodeType == minidom.Node.ELEMENT_NODE
        ]
        for element in elements:
            if element.localName == "ProcessDescription":
                self.processDescriptions.append(
                    ProcessDescription(
                        element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri
                    )
                )

    def _parseCapabilities(self, xml):
        """
        Parse les capacités du service WPSClient
        @param xml: Instance minidom des capacités
        """
        elements = [
            x
            for x in xml.documentElement.childNodes
            if x.nodeType == minidom.Node.ELEMENT_NODE
        ]
        ns_wps = PlgOptionsManager.get_plg_settings().ns_wps_uri
        ns_ows = PlgOptionsManager.get_plg_settings().ns_ows_uri
        for element in elements:
            if element.localName == "ServiceIdentification":
                self.identification = ServiceIdentification(element, ns=ns_ows)
            elif element.localName == "OperationsMetadata":
                for child in element.getElementsByTagNameNS(ns_ows, "Operation"):
                    self.operations.append(OperationMetadata(child, ns=ns_ows))
            elif element.localName == "ProcessOfferings":
                for child in element.getElementsByTagNameNS(ns_wps, "Process"):
                    self.processes.append(Process(child, ns=ns_ows))
