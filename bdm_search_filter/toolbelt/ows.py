#########################################################################
#
# Copyright (C) 2016
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

"""
Module de gestion des types de données rendus par un service WPS
"""
from xml.dom import minidom

from bdm_search_filter.toolbelt.preferences import PlgOptionsManager


def getXmlElementValue(element):
    """
    Récupère la valeur de l'unique node de l'élément
    """
    if element.hasChildNodes():
        return element.firstChild.nodeValue
    return None


class ComplexData(object):
    """
    Classe de parsing de ComplexData
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_wps_uri):
        self._element = element
        self._ns = ns

        # MimeType
        self.mimeType = element.getAttribute("mimeType")
        # type
        self.type = "ComplexData"

        # Data
        datas = [
            data.toxml()
            for data in element.childNodes
            if data.nodeType == minidom.Node.ELEMENT_NODE
        ]
        self.data = datas.pop()


class LiteralData(object):
    """
    Classe de parsing de LiteralData
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        self._element = element
        self._ns = ns

        # type
        self.type = "LiteralData"
        # dataType
        elementByTagName = element.getElementsByTagNameNS(self._ns, "DataType")
        if len(elementByTagName) >= 1:
            self.dataType = getXmlElementValue(elementByTagName[0])


class OperationMetadata(object):
    """
    Classe de parsing de OperationMetadata
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        self._element = element
        self._ns = ns

        # Name
        self.name = element.getAttribute("name")


class InputOutput(object):
    """
    Classe de parsing de Input/Output
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        self._element = element
        self._ns = ns
        self.data = []

        # Identifier
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Identifier")
        self.identifier = getXmlElementValue(elementByTagName[0])
        # Title
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Title")
        self.title = getXmlElementValue(elementByTagName[0])
        # Abstract
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Abstract")
        if len(elementByTagName) >= 1:
            self.abstract = getXmlElementValue(elementByTagName[0])

        # Complex data
        self._parseComplexData(element, "ComplexData")

        # Literal data
        self._parseLiteralData(element, "LiteralData")

    def _parseComplexData(
        self,
        element,
        complexDataTagName,
        ns_wps=PlgOptionsManager.get_plg_settings().ns_wps_uri,
    ):
        for child in element.getElementsByTagNameNS(ns_wps, complexDataTagName):
            self.data.append(ComplexData(child, ns=ns_wps))

    def _parseLiteralData(self, element, literalDataTagName):
        for child in element.getElementsByTagName(literalDataTagName):
            self.data.append(LiteralData(child, ns=self._ns))


class Output(InputOutput):
    """
    Classe de parsing de Output
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        super(Output, self).__init__(element, ns=ns)


class Input(InputOutput):
    """
    Classe de parsing de Input
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        super(Input, self).__init__(element, ns=ns)

        self.minOccurs = 0
        self.maxOccurs = 0

        if element.hasAttribute("minOccurs"):
            self.minOccurs = element.getAttribute("minOccurs")
        if element.hasAttribute("maxOccurs"):
            self.maxOccurs = element.getAttribute("maxOccurs")


class Process(object):
    """
    Classe de parsing de Process
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        self._element = element
        self._ns = ns

        # Identifier
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Identifier")
        self.identifier = getXmlElementValue(elementByTagName[0])
        # Title
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Title")
        self.title = getXmlElementValue(elementByTagName[0])
        # Abstract
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Abstract")
        self.abstract = getXmlElementValue(elementByTagName[0])


class ServiceIdentification(object):
    """
    Classe de parsing de ServiceIdentification
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        self._element = element
        self._ns = ns

        # Title
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Title")
        self.title = getXmlElementValue(elementByTagName[0])
        # ServiceType
        elementByTagName = element.getElementsByTagNameNS(self._ns, "ServiceType")
        self.serviceType = getXmlElementValue(elementByTagName[0])
        # ServiceTypeVersion
        elementByTagName = element.getElementsByTagNameNS(
            self._ns, "ServiceTypeVersion"
        )
        self.serviceTypeVersion = getXmlElementValue(elementByTagName[0])


class DataInput(object):
    """
    Classe de parsing de DataInput
    """

    def __init__(self, element):
        self.minOccurs = 0
        self.maxOccurs = 0

        if element.hasAttribute("minOccurs"):
            self.minOccurs = element.getAttribute("minOccurs")
        if element.hasAttribute("maxOccurs"):
            self.maxOccurs = element.getAttribute("maxOccurs")


class ProcessOutput(object):
    """
    Classe de parsing de ProcessOutput
    """

    def __init__(self, element):
        pass


class ProcessDescription(Process):
    """
    Classe de parsing de ProcessDescription
    """

    def __init__(
        self,
        element,
        ns=PlgOptionsManager.get_plg_settings().ns_ows_uri,
        ns_wps=PlgOptionsManager.get_plg_settings().ns_wps_uri,
    ):
        super(ProcessDescription, self).__init__(element, ns=ns)

        self.dataInputs = []
        self.processOutputs = []

        childInputs = element.getElementsByTagName("Input")
        for child in childInputs:
            self.dataInputs.append(Input(child))
        childOutputs = element.getElementsByTagNameNS(ns_wps, "ProcessOutputs")
        for child in childOutputs:
            self.processOutputs.append(Output(child, ns=ns))


class Status(object):
    """
    Classe de parsing de Status
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_wps_uri):
        self._element = element
        self._ns = ns

        # CreationTime
        self.creationTime = element.getAttribute("creationTime")
        # ProcessSucceeded
        elementByTagName = element.getElementsByTagNameNS(self._ns, "ProcessSucceeded")
        self.processSucceeded = getXmlElementValue(elementByTagName[0])


class LayerWMTS(object):
    """
    Classe de parsing de Layer WMTS
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        self._element = element
        self._ns = ns

        # Identifier
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Identifier")
        self.identifier = getXmlElementValue(elementByTagName[0])
        # Title
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Title")
        self.title = getXmlElementValue(elementByTagName[0])
        # Abstract
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Abstract")
        self.abstract = getXmlElementValue(elementByTagName[0])
        # Format
        elementByTagName = element.getElementsByTagName("Format")
        self.format = getXmlElementValue(elementByTagName[0])
        # Style
        styles = element.getElementsByTagName("Style")
        elementByTagName = styles[0].getElementsByTagNameNS(self._ns, "Identifier")
        self.style = getXmlElementValue(elementByTagName[0])
        # TileMatrixSet
        links = element.getElementsByTagName("TileMatrixSetLink")
        elementByTagName = links[0].getElementsByTagName("TileMatrixSet")
        self.tileMatrixSet = getXmlElementValue(elementByTagName[0])


class TileMatrixSet(object):
    """
    Classe de parsing de TileMatrixSet
    """

    def __init__(self, element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri):
        self._element = element
        self._ns = ns

        # Identifier
        elementByTagName = element.getElementsByTagNameNS(self._ns, "Identifier")
        self.identifier = getXmlElementValue(elementByTagName[0])
        # SupportedCRS
        elementByTagName = element.getElementsByTagNameNS(self._ns, "SupportedCRS")
        self.supportedCRS = getXmlElementValue(elementByTagName[0])
