#! python3  # noqa: E265

"""
    Main plugin module.
"""

import os

# standard library
from string import Template

# PyQGIS
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import QgsApplication, QgsAuthMethodConfig

# project
from bdm_search_filter.__about__ import __title__, __version__
from bdm_search_filter.core import BordeauxOmnisearchFilter
from bdm_search_filter.core.themes import ThemesDict
from bdm_search_filter.gui.dlg_settings import PlgOptionsFactory
from bdm_search_filter.toolbelt import PlgLogger, PlgTranslator
from bdm_search_filter.toolbelt.preferences import PlgOptionsManager

# ############################################################################
# ########## Classes ###############
# ##################################


class LocatorFilterPlugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.log = PlgLogger().log

        # translation
        plg_translation_mngr = PlgTranslator(
            tpl_filename=Template("bdm_search_filter_$locale.qm")
        )
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr
        self.configureFromEnvVar()

        if self.initSingleton():

            # install locator filter
            self.filter = BordeauxOmnisearchFilter(self.iface)
            self.iface.registerLocatorFilter(self.filter)

            if __debug__:
                self.log(
                    message=f"DEBUG -  ({__title__} {__version__}) installed.",
                    log_level=4,
                )

    def configureFromEnvVar(self):
        # Récupération des informations d'authentification avec la variable d'environnement.
        confStr = os.getenv("BD_BMQGIS")
        if confStr and len(confStr.split(";")) == 5:
            oracleLogin = confStr.split(";")[3]
            oraclePwd = confStr.split(";")[4]

            # Création d'une configuration d'authentification
            authMgr = QgsApplication.authManager()
            if "OraConn" in authMgr.availableAuthMethodConfigs():
                authMgr.removeAuthenticationConfig("OraConn")
            p_config = QgsAuthMethodConfig()
            p_config.setName("Connection Oracle BDM")
            p_config.setMethod("Basic")
            p_config.setConfig("username", oracleLogin)
            p_config.setConfig("password", oraclePwd)
            p_config.setId("OraConn")
            if p_config.isValid():
                authMgr.storeAuthenticationConfig(p_config)

            # On configure les settings avec les données de connexion
            authId = (
                "OraConn"
                if "OraConn" in QgsApplication.authManager().configIds()
                else ""
            )
            PlgOptionsManager.set_value_from_key("oracle_auth", authId)
            PlgOptionsManager.set_value_from_key("oracle_host", confStr.split(";")[0])
            PlgOptionsManager.set_value_from_key("oracle_port", confStr.split(";")[1])
            PlgOptionsManager.set_value_from_key(
                "oracle_database", confStr.split(";")[2]
            )

    def initSingleton(self) -> bool:
        """Set up plugin singletons."""
        try:
            ThemesDict()
            return True
        except Exception as error:
            self.log(
                message=(
                    "Plugin configuration error. Init ThemeDict erreur: {}".format(
                        error.args
                    )
                ),
                log_level=2,
                push=True,
            )
            return False

    def initGui(self):
        """Set up plugin UI elements."""
        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)

        # remove filter from locator
        self.iface.deregisterLocatorFilter(self.filter)
