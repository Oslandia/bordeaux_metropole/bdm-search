<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>dlg_settings</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="26"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="88"/>
        <source>Features</source>
        <translation>Fonctionnalités</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="113"/>
        <source>Request URL:</source>
        <translation>Base de l&apos;URL de requête :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="132"/>
        <source>Request parameters:</source>
        <translation>Paramètres de la requête :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="146"/>
        <source>WPS service</source>
        <translation>Service WPS :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="194"/>
        <source>HTTP user-agent:</source>
        <translation>HTTP user-agent :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="201"/>
        <source>Authentication Id</source>
        <translation>Id d&apos;authentification</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="220"/>
        <source>Minimal search length</source>
        <translation>Nombre minimal de caractères avant de déclencher la requête à l&apos;API</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="226"/>
        <source> characters</source>
        <translation> caractères</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="283"/>
        <source>HTTP content type:</source>
        <translation>HTTP content type :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="290"/>
        <source>Request key</source>
        <translation>Clé de connexion BDM :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="309"/>
        <source>Minimal search length:</source>
        <translation>Longueur minimale de la recherche :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="323"/>
        <source>WPS version</source>
        <translation>Version WPS :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="443"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="452"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="461"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Activer le mode debug (performances dégradées)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="493"/>
        <source>Open documentation</source>
        <translation>Ouvrir la documentation</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="496"/>
        <source>Help</source>
        <translation>Aide en ligne</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="503"/>
        <source>Version used to save settings:</source>
        <translation>Version des paramètres :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="522"/>
        <source>Report an issue</source>
        <translation>Signaler une anomalie</translation>
    </message>
</context>
</TS>
