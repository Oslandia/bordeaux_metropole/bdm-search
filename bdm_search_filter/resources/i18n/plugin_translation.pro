FORMS = ../../gui/dlg_settings.ui

SOURCES= ../../plugin_main.py \
    ../../core/locator_filter.py \
    ../../toolbelt/log_handler.py \
    ../../toolbelt/network_manager.py \
    ../../toolbelt/preferences.py

TRANSLATIONS = bdm_search_filter_fr.ts
