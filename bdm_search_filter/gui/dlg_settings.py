#! python3  # noqa: E265

"""
    Plugin settings form integrated into QGIS 'Options' menu.
"""

# standard library
from functools import partial
from pathlib import Path

# PyQGIS
from qgis.gui import QgsOptionsPageWidget, QgsOptionsWidgetFactory
from qgis.PyQt import uic
from qgis.PyQt.Qt import QUrl
from qgis.PyQt.QtGui import QDesktopServices, QIcon

# project
from bdm_search_filter.__about__ import (
    DIR_PLUGIN_ROOT,
    __title__,
    __uri_homepage__,
    __uri_tracker__,
    __version__,
)
from bdm_search_filter.toolbelt import PlgLogger, PlgOptionsManager
from bdm_search_filter.toolbelt.preferences import PlgSettingsStructure

# ############################################################################
# ########## Globals ###############
# ##################################

FORM_CLASS, FORM_BASE = uic.loadUiType(
    Path(__file__).parent / "{}.ui".format(Path(__file__).stem)
)

# ############################################################################
# ########## Classes ###############
# ##################################


class ConfigOptionsPage(FORM_CLASS, QgsOptionsPageWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName("mOptionsPage{}".format(__title__))
        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager()

        # header
        self.lbl_title.setText(f"{__title__} - Version {__version__}")

        # customization
        self.btn_help.setIcon(QIcon(":/images/themes/default/mActionHelpContents.svg"))
        self.btn_help.pressed.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.btn_report.setIcon(
            QIcon(":images/themes/default/console/iconSyntaxErrorConsole.svg")
        )
        self.btn_report.pressed.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_tracker__))
        )

        # load previously saved settings
        self.load_settings()

    def load_settings(self) -> dict:
        """Load options from QgsSettings into UI form."""
        settings = self.plg_settings.get_plg_settings()

        # features
        self.lbl_url_path_value.setText(settings.request_url)
        self.lbl_url_query_value.setText(settings.request_url_query)
        self.lbl_http_content_type_value.setText(settings.http_content_type)
        self.lbl_http_user_agent_value.setText(settings.http_user_agent)
        self.sbx_min_search_length.setValue(settings.min_search_length)
        self.led_request_key_value.setText(settings.request_key)
        self.lbl_wps_service_value.setText(settings.wps_service)
        self.lbl_wps_version_value.setText(settings.wps_version)
        self.led_auth_id_value.setText(settings.auth_id)
        self.lbl_ns_bm_value.setText(settings.ns_bm_uri)
        self.lbl_ns_gml_value.setText(settings.ns_gml_uri)
        self.lbl_ns_ows_value.setText(settings.ns_ows_uri)
        self.lbl_ns_wps_value.setText(settings.ns_wps_uri)

        # databases
        self.led_oracle_auth_value.setText(settings.oracle_auth)
        self.led_oracle_database_value.setText(settings.oracle_database)
        self.led_oracle_host_value.setText(settings.oracle_host)
        self.led_oracle_port_value.setText(settings.oracle_port)
        self.highlight_timer_value.setText(settings.highlight_timer)
        # misc
        self.opt_debug.setChecked(settings.debug_mode)
        self.lbl_version_saved_value.setText(settings.version)

    def apply(self):
        """Called to permanently apply the settings shown in the options page (e.g. \
        save them to QgsSettings objects). This is usually called when the options \
        dialog is accepted."""
        # save user settings
        new_settings = PlgSettingsStructure(
            min_search_length=str(self.sbx_min_search_length.value()),
            request_key=self.led_request_key_value.text(),
            auth_id=self.led_auth_id_value.text(),
            debug_mode=self.opt_debug.isChecked(),
            oracle_auth=self.led_oracle_auth_value.text(),
            oracle_database=self.led_oracle_database_value.text(),
            oracle_host=self.led_oracle_host_value.text(),
            oracle_port=self.led_oracle_port_value.text(),
            highlight_timer=self.highlight_timer_value.text(),
            version=__version__,
        )
        self.plg_settings.save_from_object(new_settings)

        if __debug__:
            self.log(
                message="DEBUG - Settings successfully saved.",
                log_level=4,
            )


class PlgOptionsFactory(QgsOptionsWidgetFactory):
    def __init__(self):
        super().__init__()

    def icon(self) -> QIcon:
        return QIcon(str(DIR_PLUGIN_ROOT / "resources/images/icon.svg"))

    def createWidget(self, parent) -> ConfigOptionsPage:
        return ConfigOptionsPage(parent)

    def title(self) -> str:
        return __title__
