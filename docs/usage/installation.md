# Installation

## Version stable (recommandée)

Le plugin est publié sur le dépôt officiel des extensions de QGIS : <https://plugins.qgis.org/plugins/bdm_search_filter/>.

## Versions expérimentales

Des versions intermédiaires (alpha, beta...) sont parfois publiées sur le dépôt officiel dans le canal expérimental.

Pour y accéder, il suffit d'activer les extensions expérimentales dans les préférences du gestionnaire d'extensions de QGIS.

## Version en développement

Si vous vous considérez comme un *early adopter*, un testeur ou que vous ne pouvez attendre qu'une version soit publiée (même dans le canal expérimental !), vous pouvez utiliser la version automatiquement packagée pour chaque commit poussé sur la branche principale.

Pour cela, il faut ajouter cette URL dans les dépôts référencés dans le gestionnaire d'extensions de QGIS :

```url
https://oslandia.gitlab.io/bordeaux_metropole/bdm-search/plugins.xml
```
