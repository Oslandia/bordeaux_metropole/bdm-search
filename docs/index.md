# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

![Demonstration of the Localisateur Bordeaux Métropole plugin for QGIS](/_static/images/bdm_omnisearch_demo.gif "Localisateur Bordeaux Métropole - demo")

----

```{toctree}
---
caption: Manuel d'utilisation
maxdepth: 1
---
Installation <usage/installation>
Utilisation <usage/how_to_use>
```

```{toctree}
---
caption: Project life
maxdepth: 1
---
misc/credits
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/translation
development/packaging
development/testing
development/history
```
