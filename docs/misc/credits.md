# Credits and sponsoring

This plugin is forked from:

- the initial work of Richard Duivenvoorde, introduced on his [blog post](https://qgis.nl/2018/05/16/english-coding-a-qgslocator-plugin/?lang=en) ;
- the [French Locator Filter](https://oslandia.gitlab.io/qgis/french_locator_filter/), by Oslandia

## Funding

```{admonition} Ready to contribute?
Plugin is free to use, not to develop. If you use it quite intensively or are interested in improving it, please consider to contribute to the code, to the documentation or fund some developments:

- [identified enhancements](https://gitlab.com/Oslandia/bordeaux_metropole/bdm-search/-/issues?scope=all&state=opened&label_name[]=enhancement)
- want to fund? Please, [send us an email](mailto:qgis@oslandia.com)
```

## Graphics

The plugin icon is [Wine by Flatart from the Noun Project](https://thenounproject.com/term/wine/2769025 ).
